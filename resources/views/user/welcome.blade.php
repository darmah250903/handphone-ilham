@extends('user.app')
@section('content')

<section class="home-banner">
  <div class="container">
    <div class="home-slider owl-carousel">
      @foreach($produks as $produk)
      <div class="banner-bg align-flax">
        <div class="w-100">
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 align-flax">
              <div class="banner-img"><img src="{{$produk->image}}" alt="banner"></div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 align-flax">
              <div class="banner-heading w-100">
                <h2 class="banner-title">{{$produk->name}}</h2>
                <p class="banner-sub">{{$produk->description}}</p>
                <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="btn">Shop now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>

<section class="featured pt-100">
  <div class="container">
    <div class="row mb-25">
      <div class="col-xl-6 col-lg-6 col-md-6">
        <div class="hading">
          <h2 class="hading-title">FEATURED <span>PRODUCTS</span></h2>
        </div>
      </div>
      <div class="col-xl-6 col-lg-6 col-md-6">
        <ul id="tabs" class="product-isotop filters-product text-right text-uppercase nav nav-tabs" role="tablist">
          @foreach($categories as $item)
          <li role="presentation" class="transition" data-filter=".item-{{$item->id}}">
            <a href="#item-{{$item->id}}" role="tab" data-toggle="tab">{{$item->name}}</a>
          </li>
          @endforeach
        </ul>
      </div>
    </div>
    <div class="tab-content">
      @foreach($categories as $key => $item )
      <div role="tabpanel" class="row tab-pane fade {{$key == 0 ? 'active show' : ''}}" id="item-{{$item->id}}">
        @foreach($item->product as $citem)
        <div class="featured-product mb-25">
          <div class="product-img transition mb-15">
            <a href="{{ route('user.produk.detail',['id' =>  $citem->id]) }}">
              <img src="{{$citem->image}}" alt="product" class="transition">
            </a>
            <div class="product-details-btn text-uppercase text-center transition">
              <a href="{{ route('user.produk.detail',['id' =>  $produk->id]) }}" class="quick-popup">Quick View</a>
            </div>
          </div>
          <div class="product-desc" data-category="accessories">
            <a href="{{ route('user.produk.detail',['id' =>  $citem->id]) }}" class="product-name text-uppercase">{{$citem->name}}</a>
            <span class="product-pricce">{{number_format($citem->price, 2, '.', ',')}}</span>
          </div>
        </div>
        @endforeach
      </div>
      @endforeach
    </div>
  </div>
</section>

    @endsection